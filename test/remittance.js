var Remittance = artifacts.require("./Remittance.sol");

const assertInvalidOpCode = (err) => {
  assert.equal(err.message, "VM Exception while processing transaction: invalid opcode");
}


contract('Remittance', function(accounts) {
  var contract;
  var owner = accounts[0];
  var alice = accounts[1]
  var bob = accounts[2]; 
  var carol = accounts[3];

  //Add 0x before hash otherwise solidity convert it
  // Yes hashing password is useless...
  var hashPassword1 = "0xabbe6325ea0d23629e7199100ba1e9ba2278c0a33a9c4bfc6cd091e5a2608f1a";
  var hashPassword2 = "0x7a4b354a32c27a83597817ec5071ebc31c4dbe3b95f47c1216af1d5a9f110c82";
  var sha3SolidityPassword12 = "0xd781e71e20825e86eb01fda32e7e7b17a2cda1b5a376499bc966580a39c2b5fc";
 
  var amountWei = web3.toWei(10, "ether");
  var gasPriceWei = web3.toWei(50, "gwei");


  beforeEach(function(){
    return Remittance.new({from: owner, value:0})
    .then(function(instance) {
      contract = instance;
    });
  });


  it("should create a Remittance and carol should collect it", function(){
    var carolBalanceBefore = web3.eth.getBalance(carol);

    return contract.createRemittance(sha3SolidityPassword12, 0, {from: alice, value: amountWei})
    .then(function(_txn) {
      return contract.remittancesList(sha3SolidityPassword12, {from: alice})
      .then(function(_remittance) {
        assert.strictEqual(_remittance[0], alice, "Alice is not the remittance owner");
        assert.equal(_remittance[2].toString(10), amountWei, "Amount has not been sent to contract");
        assert.strictEqual(_remittance[3], true, "Remittance is not set to true");
      })
      .then(function(_txn) {
        return contract.collectRemittance(hashPassword1, hashPassword2, {from: carol, gasPrice: gasPriceWei})
        .then(function(_txn) {

          // To calculate what she received minus the fee
          var fee = _txn.receipt.gasUsed * gasPriceWei;
          var totalAmount = web3.toBigNumber(amountWei).minus(fee);
          var carolCurrentBalance = web3.eth.getBalance(carol);

          assert.equal(carolCurrentBalance.toString(10),carolBalanceBefore.plus(totalAmount).toString(10), "Carol has an incorrect balance");
        });
      });
    });
  });

  it("should throw if two remittance with the same password", (done) => {
    contract.createRemittance(sha3SolidityPassword12, 0, {from: alice, value: amountWei})
    .then((txInfo) => {
      contract.createRemittance(sha3SolidityPassword12, 0, {from: bob, value: amountWei})
      .then((txInfo) => {
        done(new Error("User submitted a tx and created a remittance with zero value"));
      })
      .catch((err) => {
        assertInvalidOpCode(err);
        done();
    });
    });
  });

});
