pragma solidity ^0.4.6;

contract Remittance {

    address public owner;
    mapping(bytes32 => RemittanceStruct) public remittancesMap;

    struct RemittanceStruct {
        address owner;
        address shop;
        uint remittanceExpiration;
        uint pendingWithdrawals;
    }
    
    function Remittance()
    	public
    {
        owner = msg.sender;
    }
    
    function createRemittance(bytes32 _hashPasswords, address _shop, uint _remittanceDuration)
        public
        payable
        returns (bool success)
    {
        require(msg.value!=0);
        // Check for SHA3 collisions
        require(!isRemittance(_hashPasswords));
        
        RemittanceStruct memory remittanceStruct = RemittanceStruct({
            owner: msg.sender, 
            shop: _shop,
            remittanceExpiration: _remittanceDuration + block.number,
            pendingWithdrawals: msg.value
            });

        // Remittance creator must send a keccak256 hash of both hashed passwords
        remittancesMap[_hashPasswords] = remittanceStruct;
        return true;
    }
    
    function collectRemittance(bytes32 _hashPassword1, bytes32 _hashPassword2)
        public
        returns (bool success)
    {
        // The hash of the two hashed passwords should match the hash given by the creator
        bytes32 remittancePassword = keccak256(_hashPassword1, _hashPassword2);
        require(isRemittance(remittancePassword));
        
        RemittanceStruct memory remittanceCollected = remittancesMap[remittancePassword];
        
        // Creator of remittance can withdraw funds only if expiration date has passed
        // In case of failed transaction everyone could know the passwords and send it again faster for ex.)
        if (block.number < remittanceCollected.remittanceExpiration)
        {
            require(msg.sender == remittanceCollected.shop);
        } else {
            // Creator of remittance comes withdraw expired remittance
            // Owner of contract can also withdraw it, if creator never withdraw.
            require(msg.sender == remittanceCollected.owner || msg.sender == owner);
        }
        
        uint amount = remittanceCollected.pendingWithdrawals;
        remittanceCollected.pendingWithdrawals = 0;
        msg.sender.transfer(amount);
        return true;
    }

    function isRemittance(bytes32 _hashPasswords) 
        public 
        constant 
        returns(bool isIndeed)
    {
        return remittancesMap[_hashPasswords].owner != 0;
    }
    
     function killSwitch()
        public
    {
        require(msg.sender == owner);
        selfdestruct(owner);
    }
    
}
